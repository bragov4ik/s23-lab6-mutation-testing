# syntax=docker/dockerfile:1

FROM python:3.10-slim-buster

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY src src
WORKDIR /src

CMD ["mutatest", "--src" , "./bonus_system.py", "--testcmds", "pytest ./test_bonus_system.py", "-x", "1"]