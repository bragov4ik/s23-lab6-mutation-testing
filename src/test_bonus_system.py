import pytest
from bonus_system import calculateBonuses;

@pytest.mark.parametrize(
    "amount,bonus", (
        (-31337, 1),
        (0, 1),
        (9999, 1),
        (10000, 1.5),
        (10001, 1.5),
        (25000, 1.5),
        (49999, 1.5),
        (50000, 2),
        (50001, 2),
        (75000, 2),
        (99999, 2),
        (100000, 2.5),
        (100001, 2.5),
        (150000, 2.5),
        (9999999999, 2.5),
    )
)
def test_valid_bonus(amount, bonus):
    programs = [
        ("Standard", 0.5),
        ("Premium", 0.1),
        ("Diamond", 0.2),
        ("", 0)
    ]
    for program, multiplier in programs:
        expected = bonus*multiplier
        assert expected == calculateBonuses(program, amount)

def test_none():
    assert calculateBonuses(None, 0) == 0

def test_invalid():
    assert calculateBonuses("aboba", 0) == 0
